import XCTest
@testable import UsersList

fileprivate class MockStorageLogger: StorageLogging {
    
    private(set) var readCount = 0
    private(set) var writeCount = 0
    
    func logRead() {
        readCount += 1
    }
    
    func logWrite() {
        writeCount += 1
    }
}

fileprivate class MockUserDefaults : UserDefaults {
    override init?(suiteName suitename: String?) {
        guard let suitename = suitename else {
            return nil
        }
        super.init(suiteName: suitename)
        UserDefaults().removePersistentDomain(forName: suitename)
    }
}

final class StorageTests: XCTestCase {
    
    private var storage: UserStorageServiceProtocol?
    private var logger: MockStorageLogger?
    private let user = User(gender: .male, email: "ant@gmail.com", title: "Mr", first: "Anton", last: "Krasilnikov", image: "123")
    
    override func setUp() {
        super.setUp()
        logger =  MockStorageLogger()
        let mockUserDefaults: UserDefaults = MockUserDefaults(suiteName: "testing") ?? UserDefaults.standard
        let userDefaults = UserDefaultsService(store: mockUserDefaults, logger: logger ?? MockStorageLogger())
        storage = UserStorageService(userDefaultsService: userDefaults)
    }
    
    func testGettingAnEmptyValue() throws {
        
        // Given
        let users = storage?.get()
        
        // Then
        XCTAssert(users == [])
    }
    
    func testGettingValue() throws {
        
        // Given
        storage?.append(user)
        
        // When
        let users = storage?.get()
        
        // Then
        XCTAssert(users == [user])
    }
    
    func testAppendUser() throws {
        
        // When
        storage?.append(user)
        let users = storage?.get()
        let isContains = users?.contains(user)
        
        // Then
        XCTAssert(isContains == true)
    }
    
    func testDeleteUser() throws {
        
        // Given
        storage?.append(user)
        
        // When
        storage?.delete(user)
        let users = storage?.get()
        let isContains = users?.contains(user)
        
        // Then
        XCTAssert(isContains == false)
    }
    
    func testEditUser() throws {
        
        // Given
        storage?.append(user)
        var editedUser = user
        
        // When
        editedUser.title = "Super"
        storage?.edit(editedUser)
        let users = storage?.get()
        
        // Then
        XCTAssert(users?.first?.title == editedUser.title)
    }
    
    func testAmountOfCallsLogReadInAppendFunctionWhenStorageIsEmpty() throws {
        
        // When
        storage?.append(user)
        let count = logger?.readCount
        
        // Then
        XCTAssert(count == 0)
    }
    
    func testAmountOfCallsLogReadInAppendFunctionWhenStorageNotEmpty() throws {
        
        // Given
        storage?.append(user)
        
        // When
        storage?.append(user)
        let newCounter = logger?.readCount
        
        // Then
        XCTAssert(newCounter == 1)
    }
    
    func testNumberOfCallsLogReadWithSeveralMethods() throws {
        
        // Given
        storage?.append(user)
        
        // When
        storage?.get()
        storage?.edit(user)
        storage?.delete(user)
        
        let counter = logger?.readCount
        
        // Then
        XCTAssert(counter == 3)
    }
    
    func testNumberOfCallsLogWrite() throws {
        
        // When
        storage?.append(user)
        storage?.append(user)
        let count = logger?.writeCount
        
        // Then
        XCTAssert(count == 2)
    }
    
    func testForDeleteMethodsWithWrongUserId() throws {
        
        // Given
        storage?.append(user)
        var wrongUser = user
        wrongUser.id = UUID()
        
        // When
        storage?.delete(wrongUser)
        let users = storage?.get()?.count
        
        // Then
        XCTAssert(users != 0)
    }
    
    func testNumberOfCallsLogWriteForDeleteMethodsWithWrongUserId() throws {
        
        // Given
        storage?.append(user)
        var wrongUser = user
        wrongUser.id = UUID()
        
        // When
        storage?.delete(wrongUser)
        let count = logger?.writeCount
        
        // Then
        XCTAssert(count == 1)
    }
}
