// MARK: - LoadState
enum LoadState<T> {
    case unload
    case loading
    case done(T)
    case notFound
}
