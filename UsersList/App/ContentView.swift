import SwiftUI

struct ContentView: View {
    
    var body: some View {
        TabView {
            UserListModuleBuilder.build()
                .tabItem {
                    Label("Users", systemImage: "person.crop.circle")
                }
            
            SearchUserModuleBuilder.build()
                .tabItem {
                    Label("Search", systemImage: "magnifyingglass")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
