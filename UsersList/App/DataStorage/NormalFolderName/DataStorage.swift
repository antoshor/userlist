import Foundation
import Combine

final class DataStorage: BaseStorage {
  
    func delete(byIndex index: Int) {
        guard var data = get() else {
            return
        }
        
        data.remove(at: index)
        set(value: data, forKey: StorageKey.users)
    }
//    
//    func set(_ object: Any?, forKey key: StorageKey) {
//        DataStorage.store.set(object, forKey: key.rawValue)
//    }
//    
//    func get(forKey key: StorageKey) -> Any? {
//        DataStorage.store.object(forKey: key.rawValue)
//    }
    
    
    
    func get() -> [User]? {
        get(forKey: StorageKey.users)
    }
    
    func update(user: [User]) {
        set(value: user, forKey: StorageKey.users)
    }
}

extension DataStorage {
    static var store: UserDefaults {
        UserDefaults.standard
    }
    
    static var users: String {
        return "users"
    }
}
