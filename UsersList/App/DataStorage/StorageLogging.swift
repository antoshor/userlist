import Foundation

protocol StorageLogging {
    
    func logRead()
    
    func logWrite()
}

final class StorageLogger: StorageLogging {
    
    func logRead() {
        print(#function)
    }
    
    func logWrite() {
        print(#function)
    }
}
