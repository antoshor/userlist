import SwiftUI

protocol UserDefaultsServiceProtocol {
    
    func set<T: Encodable>(value: T, forKey key: StorageKey) throws
    
    func get<T: Decodable>(forKey key: StorageKey) throws -> T
}

final class UserDefaultsService: UserDefaultsServiceProtocol {
    
    private let store: UserDefaults
    private let logger: StorageLogging
    
    init(store: UserDefaults, logger: StorageLogging = StorageLogger()) {
        self.store = store
        self.logger = logger
    }
    
    func get<T: Decodable>(forKey key: StorageKey) throws -> T {
        guard let object = store.object(forKey: key.rawValue) else {
            throw UserDefaultsServiceError.objectNotFound(key.rawValue)
        }
        
        guard let data = object as? Data else {
            throw UserDefaultsServiceError.dataCastingError("\(object)")
        }
        
        let decoder = JSONDecoder()
        guard let decodedData = try? decoder.decode(T.self, from: data) else {
            throw  UserDefaultsServiceError.decodingError("\(T.self)")
        }
        
        logger.logRead()
        return decodedData
    }
    
    func set<T: Encodable>(value: T, forKey key: StorageKey) throws {
        let encoder = JSONEncoder()
        
        if let encodedData = try? encoder.encode(value) {
            store.set(encodedData, forKey: key.rawValue)
            logger.logWrite()
        } else {
            throw UserDefaultsServiceError.encodingError("\(T.self)")
        }
    }
}

enum StorageKey: String {
    case users = "users"
}

enum UserDefaultsServiceError: Error, CustomStringConvertible {
    case dataCastingError(String)
    case objectNotFound(String)
    case decodingError(String)
    case encodingError(String)
    
    var description: String {
        switch self {
        case .dataCastingError(let object):
            return "Error casting \(object) to type Data"
        case .objectNotFound(let key):
            return("Object for key '\(key)' not founded")
        case .decodingError(let type):
            return "Error decoding data for type: \(type)"
        case .encodingError(let type):
            return "Error encoding data for type: \(type)"
        }
    }
}


