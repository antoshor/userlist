import Foundation

protocol UserStorageServiceProtocol {
    
    func get() -> [User]?
    
    func append(_ user: User)
    
    func delete(_ user: User)
    
    func edit(_ user: User)
}

final class UserStorageService: UserStorageServiceProtocol {
    
    let userDefaultsService: UserDefaultsServiceProtocol
    
    init(userDefaultsService: UserDefaultsServiceProtocol) {
        self.userDefaultsService = userDefaultsService
    }
    
    func append(_ user: User) {
        guard var users = get() else {
            return
        }
        
        users.append(user)
        setUsers(users: users, forKey: .users)
    }
    
    func delete(_ user: User) {
        guard var users = get() else {
            return
        }
        
        if let index = checkIndexRange(forUsers: users, byUser: user){
            users.remove(at: index)
            setUsers(users: users, forKey: .users)
        }
    }
    
    func edit(_ user: User) {
        guard var users = get() else {
            return
        }
            
        if let index = checkIndexRange(forUsers: users, byUser: user) {
            users[index] = user
            setUsers(users: users, forKey: .users)
        }
    }
    
    func get() -> [User]? {
        do {
            return try userDefaultsService.get(forKey: .users)
            
        } catch UserDefaultsServiceError.objectNotFound(let key) {
            print(UserDefaultsServiceError.objectNotFound(key))
            return [User]()
        } catch {
            print(error)
            return nil
        }
    }
    
    private func setUsers(users: [User], forKey key: StorageKey) {
        do {
            try userDefaultsService.set(value: users, forKey: key)
        } catch {
            print(error)
        }
    }
    
    private func checkIndexRange(forUsers users: [User], byUser user: User) -> Int? {
        
        guard let index = users.firstIndex(where:{ $0.id == user.id }) else {
            print("Error: Index for this user not found")
            return nil
        }
        
        return index
    }
}
