import SwiftUI

enum DetailModuleBuilder {
    static func build(user: User, callback: @escaping ()->()) -> some View {
        let userDefauls = UserDefaultsService(store: .standard)
        let dataStorage = UserStorageService(userDefaultsService: userDefauls)
        let viewModel = DetailViewModel(user: user, dataStorage: dataStorage, lastSavedUser: user, callback: callback)
        
        return DetailView(viewModel: viewModel)
    }
}
