import SwiftUI

struct DetailView: View {
    
    @Environment(\.editMode) var editMode
    @ObservedObject var viewModel: DetailViewModel
    
    var body: some View {
        VStack {
            if editMode?.wrappedValue == .inactive {
                AsyncImageView(viewModel.user.image)
                    .cornerRadius(10)
                
                HStack {
                    Text(viewModel.user.title)
                    Text(viewModel.user.first)
                    Text(viewModel.user.last)
                }
                .lineLimit(1)
                .font(.title)
                
                Text(viewModel.user.gender.rawValue)
                    .font(.subheadline)
                    .foregroundColor(.green)
                
                Text(viewModel.user.email)
                    .font(.subheadline)
                    .opacity(0.6)
                
            } else {
                EditUserView(user: $viewModel.user)
            }
        }
        .toolbar {
            HStack {
                if editMode?.wrappedValue == .active {
                    Button {
                        viewModel.user = viewModel.lastSavedUser
                        editMode?.wrappedValue = .inactive
                    } label: {
                        Text("Cancel")
                    }
                }
                
                Spacer()
                Button {
                    if editMode?.wrappedValue == .active  {
                        viewModel.lastSavedUser = viewModel.user
                        viewModel.editUser()
                        editMode?.wrappedValue = .inactive
                    } else {
                        editMode?.wrappedValue = .active
                    }
                } label: {
                    Text("Edit")
                }
            }
        }
        .onDisappear(perform: {
            viewModel.user = viewModel.lastSavedUser
        })
    }
}


