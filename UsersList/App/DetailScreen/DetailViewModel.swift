import Foundation

final class DetailViewModel: ObservableObject {
    
    @Published var user: User
    @Published var lastSavedUser: User
    
    var callback: () -> Void
    
    private let dataStorage: UserStorageServiceProtocol
    
    init(
        user: User,
        dataStorage: UserStorageServiceProtocol,
        lastSavedUser: User,
        callback: @escaping () -> Void
    ) {
        self.user = user
        self.dataStorage = dataStorage
        self.callback = callback
        self.lastSavedUser = lastSavedUser
    }
    
    func editUser() {
        dataStorage.edit(user)
        callback()
    }
}
