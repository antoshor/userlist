import SwiftUI

struct EditUserView: View {
    
    @Binding var user: User
    
    var body: some View {
        List {
            TextView(text: $user.title, title: "Title")
            
            TextView(text: $user.first, title: "Firstname")
            
            TextView(text: $user.last, title: "Lastname")
            
            TextView(text: $user.email, title: "Email")
            
            Picker("Gender", selection: $user.gender) {
                ForEach(User.Gender.allCases) { gender in
                    Text(gender.rawValue).tag(gender)
                }
            }
        }
    }
}
