import SwiftUI

struct TextView: View {
    
    @Binding var text: String
    
    let title: String
    
    var body: some View {
        HStack {
            Text(title)
            Spacer()
            TextField(title.lowercased(), text: $text)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.trailing)
        }
    }
}
