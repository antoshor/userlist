import Foundation
import Combine

protocol BaseNetworkService {
    func fetch<T: Decodable>(withUrl url: String) -> AnyPublisher<T, Error>
}

extension BaseNetworkService {
    func fetch<T: Decodable>(withUrl url: String) -> AnyPublisher<T, Error> {
        guard let newUrl = URL(string: url) else  {
            return Fail(error: NetworkError.invalidURL(url))
                .eraseToAnyPublisher()
        }
        
        let request =  URLRequest(url: newUrl)
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .receive(on: DispatchQueue.main)
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

enum NetworkError: Error, CustomStringConvertible {
    case invalidURL(String)
    case emptyData(String)
    case anotherError(Error)
    case encodeUrlError(String)
    
    var description: String {
        switch self {
        case .invalidURL(let url):
            return "invalidURL: \(url)"
        case .emptyData(let data):
            return "emptyData: \(data)"
        case .anotherError(let error):
            return "anotherError: \(error)"
        case .encodeUrlError(let url):
            return "can't encode url: \(url)"
        }
    }
}
