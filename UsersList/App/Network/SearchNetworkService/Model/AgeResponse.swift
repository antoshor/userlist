// MARK: - AgeResponse
struct AgeResponse: Codable {
    let age: Int?
}
