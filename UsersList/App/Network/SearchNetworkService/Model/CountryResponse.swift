// MARK: - CountryResponse
struct CountryResponse: Codable {
    let country: [Country]
    
    // MARK: - Country
    struct Country: Codable {
        let countryID: String
        let probability: Double

        enum CodingKeys: String, CodingKey {
            case countryID = "country_id"
            case probability
        }
    }
}

