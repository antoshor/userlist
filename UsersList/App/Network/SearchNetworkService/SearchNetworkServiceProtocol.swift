import Combine

protocol SearchNetworkServiceProtocol {
    
    func getAge(forName name: String) -> AnyPublisher<AgeResponse, Error>
    
    func getGender(forName name: String) -> AnyPublisher<GenderResponse, Error>
    
    func getCountry(forName name: String) -> AnyPublisher<CountryResponse, Error>
}
