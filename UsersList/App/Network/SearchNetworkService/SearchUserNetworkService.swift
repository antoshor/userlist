import Combine
import Foundation

class SearchUserNetworkService: SearchNetworkServiceProtocol, BaseNetworkService {
    
    func getAge(forName name: String) -> AnyPublisher<AgeResponse, Error> {
        
        return fetch(withUrl: SearchUserNetworkService.ageUrl + name)
    }
    
    func getGender(forName name: String) -> AnyPublisher<GenderResponse, Error> {
        
        return fetch(withUrl: SearchUserNetworkService.genderUrl + name)
    }
    
    func getCountry(forName name: String) -> AnyPublisher<CountryResponse, Error> {

        return fetch(withUrl: SearchUserNetworkService.countryUrl + name)
    }
}

extension SearchUserNetworkService {
    static var ageUrl: String {
        "https://api.agify.io?name="
    }
    
    static var genderUrl: String {
        "https://api.genderize.io?name="
    }
    
    static var countryUrl: String {
        "https://api.nationalize.io/?name="
    }
}
