// MARK: - UserResponse
struct UserResponse: Codable {
    let results: [Person]
}

// MARK: - Person
struct Person: Codable, Hashable {
    let gender: String
    let name: Name
    let location: Location
    let email: String
    let picture: Picture
    
    // MARK: - Location
    struct Location: Codable, Hashable {
        let city: String
        let postcode: PostcodeType
    }
    
    // MARK: - Name
    struct Name: Codable, Hashable {
        let title: String
        let first: String
        let last: String
    }
    
    // MARK: - Picture
    struct Picture: Codable, Hashable {
        let large: String
        let medium: String
        let thumbnail: String
    }
    
    // MARK: - PostcodeType
    enum PostcodeType: Codable, Hashable {
        case int(Int)
        case string(String)
        
        init(from decoder: Decoder) throws {
            if let intValue = try? decoder.singleValueContainer().decode(Int.self) {
                self = .int(intValue)
            } else if let stringValue = try? decoder.singleValueContainer().decode(String.self) {
                self = .string(stringValue)
            } else {
                throw DecodingError.dataCorruptedError(
                    in: try decoder.singleValueContainer(),
                    debugDescription: "Could not decode postcode"
                )
            }
        }
    }
}
