import Combine

protocol UserNetworkServiceProtocol {
    
    func getUser() -> AnyPublisher<Person, Error>
}
