import Combine

class UserNetworkService: UserNetworkServiceProtocol, BaseNetworkService {
    
    func getUser() -> AnyPublisher<Person, Error> {
        
        return fetch(withUrl: UserNetworkService.randomuserUrl)
            .tryMap { (value: UserResponse) -> Person in
                guard let value = value.results.first else {
                    throw NetworkError.emptyData(String(describing: value.results.first))
                }
                
                return value
            }
            .eraseToAnyPublisher()
    }
}

extension UserNetworkService {
    static var randomuserUrl: String {
        "https://randomuser.me/api/"
    }
}
