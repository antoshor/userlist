struct SearchedPerson {
    var age: String
    var gender: String
    var countries: [Country]
    
    struct Country {
        let countryID: String
        let probability: String
        
        init(countryID: String, probability: Double) {
            self.countryID = countryID
            self.probability = "\(String(format: "%.1f", probability * 100)) %"
        }
    }
}

extension SearchedPerson {
    init(
        age: Int?,
        gender: String?,
        countries: [CountryResponse.Country]
    ) {
        let age: String = {
            if let age = age {
                return String(age)
            } else {
                return "not found"
            }
        }()
        
        let gender: String = {
            if let gender = gender {
                return String(gender)
            } else {
                return "not found"
            }
        }()
        
        self.age = age
        self.gender = gender
        self.countries = countries.map({ value in
            Country(countryID: value.countryID, probability: value.probability)
        })
    }
}
