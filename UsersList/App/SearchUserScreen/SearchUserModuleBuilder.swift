import SwiftUI

enum SearchUserModuleBuilder {
    static func build() -> some View {
        let networkService = SearchUserNetworkService()
        let viewModel = SearchUserViewModel(networkService: networkService)
        
        return SearchUserView(viewModel: viewModel)
    }
}
