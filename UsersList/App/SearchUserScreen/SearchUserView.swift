import SwiftUI

struct SearchUserView: View {
    
    @ObservedObject var viewModel: SearchUserViewModel
    
    @State var text: String = ""
    
    var body: some View {
        NavigationView {
            VStack {
                SearchFieldView(text: $text, viewModel: viewModel)
                
                switch viewModel.loadState {
                case .unload:
                    EmptyView()
                case .loading:
                    ProgressView()
                        .padding()
                case .notFound:
                    Text("Not found")
                        .padding()
                        .font(.headline)
                case .done(let person):
                        VStack(alignment: .leading) {
                            Text("Name: \(text)")
                            Text("Gender: \(person.gender)")
                            Text("Age: \(person.age)")
                            Divider()
                        }
                        .padding(.horizontal)
                        .font(.headline)
                        
                    SearchListView(countries: person.countries)
                }
                Spacer()
            }
            .navigationTitle("Search")
        }
    }
}

struct SearchUserView_Previews: PreviewProvider {
    static var previews: some View {
        SearchUserView(viewModel: SearchUserViewModel(networkService: SearchUserNetworkService()))
    }
}
