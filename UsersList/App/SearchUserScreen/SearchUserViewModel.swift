import Combine

final class SearchUserViewModel: ObservableObject {
    
    @Published var loadState: LoadState<SearchedPerson> = .unload
    
    private var cancellables = Set<AnyCancellable>()
    private let networkService: SearchNetworkServiceProtocol
    
    init(networkService: SearchNetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func getUserInfo(forName name: String) {
        
        loadState = .loading
        
        networkService.getAge(forName: name)
            .combineLatest(
                networkService.getGender(forName: name),
                networkService.getCountry(forName: name)
            )
            .mapError({ error in
                print(error)
                return NetworkError.anotherError(error)
            })
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.loadState = .notFound
                    print("Failed with error: \(error)")
                    return
                case .finished:
                    print("Succeesfully finished!")
                }
            }, receiveValue: { [weak self] age, gender, countries in
                let person = SearchedPerson(
                    age: age.age,
                    gender: gender.gender,
                    countries: countries.country
                )
                self?.loadState = .done(person)
            })
            .store(in: &cancellables)
    }
}
