import SwiftUI

struct SearchFieldView: View {
    
    @Binding var text: String
    @ObservedObject var viewModel: SearchUserViewModel
    
    var body: some View {
        HStack {
            TextField("Enter the name", text: $text) { changed in
                print(changed)
            } onCommit: {
                viewModel.getUserInfo(forName: text)
            }
            .padding()
            .foregroundColor(.gray)
            .background(Color.white)
            .cornerRadius(12)
            .shadow(color: .gray, radius: 3, x: 2, y: 2)
        }
        .padding()
    }
}
