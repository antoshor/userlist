import SwiftUI

struct SearchListView: View {
    
    let countries: [SearchedPerson.Country]
    
    var body: some View {
        VStack(alignment: .leading) {
            List(countries, id: \.countryID) { country in
                SearchUserRow(country: country)
            }
            .listStyle(.plain)
        }
    }
}

struct SearchListView_Previews: PreviewProvider {
    static var previews: some View {
        SearchListView(countries:
                        [
                            SearchedPerson.Country(countryID: "USA", probability: 0.5),
                            SearchedPerson.Country(countryID: "USA", probability: 0.5),
                            SearchedPerson.Country(countryID: "USA", probability: 0.5)
                        ]
        )
    }
}
