import SwiftUI

struct SearchUserRow: View {
    
    let country: SearchedPerson.Country
    
    var body: some View {
        HStack {
            Spacer()
            VStack{
                Text("Сountry: \(country.countryID)")
                Text("Probability: \(country.probability)")
            }
            .font(.headline)
            Spacer()
            
        }
        .font(.headline)
        .padding()
        .foregroundColor(.white)
        .background(Color.blue)
        .cornerRadius(12)
    }
}

struct SearchUserRow_Previews: PreviewProvider {
    static var previews: some View {
        SearchUserRow(country: SearchedPerson.Country(countryID: "USA", probability: 0.90))
    }
}
