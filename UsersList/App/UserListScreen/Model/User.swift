import Foundation

struct User: Codable, Hashable, Identifiable {
    var gender: Gender
    var email: String
    var title: String
    var first: String
    var last: String
    let image: String
    var id = UUID()
    
    enum Gender: String, CaseIterable, Identifiable, Codable {
        case male
        case female
        case none
        
        var id: String { rawValue }
    }
}

extension User {
    init(person: Person) {
        if let gender = Gender(rawValue: person.gender) {
            self.gender = gender
        } else {
            self.gender = .none
        }
        
        self.email = person.email
        self.title = person.name.title
        self.first = person.name.first
        self.last = person.name.last
        self.image = person.picture.large
    }
}
