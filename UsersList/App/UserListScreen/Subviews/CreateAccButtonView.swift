import SwiftUI

struct CreateAccButtonView: View {
    
    @ObservedObject var modelList: UserListViewModel
    
    var body: some View {
        Button {
            modelList.fetchUsers()
        } label: {
            ZStack {
                Capsule()
                    .frame(height: 62)
                Text("+ Add new account")
                    .foregroundColor(.white)
            }
        }
        .frame(height: 50)
        .padding()
    }
}

struct CreateAccButtonView_Previews: PreviewProvider {
    static var previews: some View {
        CreateAccButtonView(
            modelList: UserListViewModel(
                networkService: UserNetworkService(),
                dataStorage: UserStorageService(
                    userDefaultsService: UserDefaultsService(
                        store: .standard,
                        logger: StorageLogger()
                    )
                )
            )
        )
    }
}


