import SwiftUI

struct ListView: View {
    
    @ObservedObject var viewModel: UserListViewModel
    
    var body: some View {
        List {
            ForEach(viewModel.users) { user in
                NavigationLink {
                    DetailModuleBuilder.build(
                        user: user,
                        callback: viewModel.updateUsers
                    )
                } label: {
                    RowView(user: user)
                }
            }
            .onDelete(perform: delete)
        }
    }
    
    private func delete(with indexSet: IndexSet) {
        viewModel.deleteUser(byIndex: indexSet.first)
    }
}

struct RowView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(
            viewModel: UserListViewModel(
                networkService: UserNetworkService(),
                dataStorage: UserStorageService(
                    userDefaultsService: UserDefaultsService(
                        store: .standard,
                        logger: StorageLogger()
                    )
                )
            )
        )
    }
}
