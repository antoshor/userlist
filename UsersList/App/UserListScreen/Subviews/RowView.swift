import SwiftUI

struct RowView: View {
    
    let user: User
 
    var body: some View {
        
        HStack {
            Text(user.title)
            Text(user.first)
            Text(user.last)
        }
    }
}


