import Foundation
import SwiftUI

enum UserListModuleBuilder {
    static func build() -> some View {
        let network = UserNetworkService()
        let userStorageService = UserDefaultsService(store: .standard)
        let storage = UserStorageService(userDefaultsService: userStorageService)
        let viewModel = UserListViewModel(networkService: network, dataStorage: storage)
        
        return UserListView(viewModel: viewModel)
    }
}
