import SwiftUI

struct UserListView: View {
    
    @ObservedObject var viewModel: UserListViewModel
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    ListView(viewModel: viewModel)
                    Spacer()
                    CreateAccButtonView(modelList: viewModel)
                }
                
                .padding()
                
                if viewModel.isLoading {
                    ProgressView()
                }
            }
        }
    }
}

struct UserListView_Previews: PreviewProvider {
    static var previews: some View {
        UserListView(
            viewModel: UserListViewModel(
                networkService: UserNetworkService(),
                dataStorage: UserStorageService(
                    userDefaultsService: UserDefaultsService(
                        store: .standard,
                        logger: StorageLogger()
                    )
                )
            )
        )
    }
}
