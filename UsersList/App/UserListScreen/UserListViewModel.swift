import Foundation
import Combine

final class UserListViewModel: ObservableObject {
    
    @Published var isLoading = false
    @Published var users = [User]()
    
    private var cancellables = Set<AnyCancellable>()
    private let networkService: UserNetworkServiceProtocol
    private let dataStorage: UserStorageServiceProtocol
    
    init(networkService: UserNetworkServiceProtocol, dataStorage: UserStorageServiceProtocol) {
        self.networkService = networkService
        self.dataStorage = dataStorage
        
        updateUsers()
    }
    
    func fetchUsers() {
        isLoading = true
        
        networkService.getUser()
            .sink(receiveCompletion: { response in
                switch response {
                case .failure(let error):
                    print("Failed with error: \(error)")
                    return
                case .finished:
                    print("Succeesfully finished!")
                }
            }, receiveValue: { [weak self] value in
                guard let self = self else {
                    return
                }
                let user = User(person: value)
                isLoading = false
                dataStorage.append(user)
                updateUsers()
            })
            .store(in: &cancellables)
    }
    
    func deleteUser(byIndex index: Int?) {
        guard let index = index, index <= users.count - 1 else {
            print("index: \(String(describing: index)) not found")
            return
        }
        
        dataStorage.delete(users[index])
        updateUsers()
    }
    
    func updateUsers() {
        if let users = dataStorage.get() {
            self.users = users
        }
    }
}
