import SwiftUI

@main
struct UsersListApp: App {
   
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
