import SwiftUI
import Combine

struct AsyncImageView: View {
    
    @ObservedObject private var imageLoader: ImageLoader
    
    init(_ url: String) {
        self.imageLoader = ImageLoader(url)
    }
    
    var body: some View {
        switch imageLoader.loadStatus {
        case .done(let image):
            Image(uiImage: image)
        case .loading:
            ProgressView()
        case .unload:
            EmptyView()
        case .notFound:
            Text("Not found")
        }
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        AsyncImageView("https://randomuser.me/api/portraits/med/men/75.jpg")
    }
}
