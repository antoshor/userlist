import Combine
import UIKit

class ImageLoader: ObservableObject {
    
    @Published var loadStatus = LoadState<UIImage>.unload
    
    private var cancellable: AnyCancellable?
    
    init(_ url: String) {
        loadStatus = .loading
        guard let url = URL(string: url) else {
            return
        }
        
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .compactMap { UIImage(data: $0) }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] response in
                switch response {
                case .failure(let error):
                    print("Failed with error: \(error)")
                    self?.loadStatus = .notFound
                    return
                case .finished:
                    print("Succeesfully finished!")
                }
            }, receiveValue: { [weak self] value in
                self?.loadStatus = .done(value)
            })
    }
}
